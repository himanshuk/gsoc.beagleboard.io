#!/bin/bash -xe
mkdir -p public

echo "**** Updating $PAGES_URL/$VER_DIR ****"
echo "**** env ****"
env

# Clean build directory
make clean BUILDDIR=public

# Render HTML
make html BUILDDIR=public
mv public/html/* public/

# Render PDF
make latexpdf BUILDDIR=public/
mv public/latex/*.pdf public/proposals

# Cleanup
echo "**** cleanup ****"
rm -rf public/doctrees
rm -rf public/latex

echo "**** env ****"
env